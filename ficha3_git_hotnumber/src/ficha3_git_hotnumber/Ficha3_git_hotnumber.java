package ficha3_git_hotnumber;

import java.util.Random;
import java.util.Scanner;

public class Ficha3_git_hotnumber {

    public static void main(String[] args) {
        int n = 0, aleatorio = 0, res = 0, ten = 0, jogadores = 0;

        Scanner ler = new Scanner(System.in);
        Random random = new Random();

        aleatorio = random.nextInt(101);
        System.out.println("Introduza o numero de jogadores");
        jogadores = ler.nextInt();

        System.out.println("Introduza as tentativas que quer utilizar:");
        ten = ler.nextInt();

        // Outra forma de fazer o que está em cima
        // res=Math.abs(n - aleatorio);
        // Para inicializar o array que nos vai controlar se o jogador acertou ou não
        boolean[] arrJogadores = new boolean[jogadores];
        for (int i = 0; i < arrJogadores.length; i++) {
            arrJogadores[i] = false;

        }
 
        for (int i = 0; i < ten; i++) {
            for (int j = 0; j < jogadores; j++) {
                arrJogadores[j] = false;
                System.out.println("Jogador " + (j + 1));
                System.out.println("Introduza um numero entre 0 e 100:");
                n = ler.nextInt();
                res = n - aleatorio;
                if (res < 0) {
                    res = aleatorio - n;
                }

                if (res == 0) {
                    System.out.println("Acertou no numero");
                    arrJogadores[j] = true;
                } else if (res <= 25 && res >= 5) {
                    System.out.println("Morno");
                } else if (res > 25) {
                    System.out.println("Frio");
                } else if (res < 5) {
                    System.out.println("Quente");
                }

            }
        }
    }

}
